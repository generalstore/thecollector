# Ludum Dare 36 - 2016

## The Collector

- 2D top down dungeon crawler
- player v. environment
- collect as many arifacts as possible
- escape the temple

### Building the game

		python -m venv _env
		source ./_env/bin/activate
		pip install -r requirements.txt
		pyinstaller game.py

### Running the game

		python game.py
		
or

		./dist/game/game


### Controls

- WASD - Movement
- Hold L-shift - Run
- ESC - exit

### Music

[Beyond the Stars](https://machinimasound.com/music/beyond-the-stars/) by Per Kiilstofte


### Dev Environment
- Python 3.10 
- pytmx 
- pygame 
- pyinstaller 

- Tiled - Map Editor 
- Vim - Programming 
- Gimp - art 
- Audacity - sound 
